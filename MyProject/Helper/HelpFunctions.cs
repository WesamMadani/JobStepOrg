﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace MyProject.Helper
{
    public static class HelpFunctions
    {
        public static string Cut(string txt, int len)
        {

            string[] textarray = txt.Split(' ');
            string res = "";


            for (int i = 0; i < len; i++)
            {
                res += textarray[i] + " ";
            }
            return res;
        }
        public static string StripTagsRegex(string strHtml)
        {
            string ss = strHtml;
            Regex regex = new Regex("\\<[^\\>]*\\>");

            ss = regex.Replace(ss, String.Empty);

            return ss;

        }
        public static string CreateNameImage(HttpPostedFileBase Image)
        {
            if (Image == null)
            {
                return ("");
            }
            string guid = Guid.NewGuid().ToString("N").ToLower();
            string Extenstion = Image.FileName.Split('.')[1];
            if (!(Extenstion == "jpg" || Extenstion == "png" || Extenstion == "gif"))
            {
                return ("");
            }
            string filename = guid + '.' + Extenstion;

            return (filename);
        }
        public static DateTime EndDate(int n)
        {
            DateTime dateTime = new DateTime();
        dateTime = DateTime.Now;
         
        DateTime newDateTime = new DateTime();
        TimeSpan NumberOfDays = new TimeSpan(n, 0, 0, 0, 0);
        newDateTime = dateTime.Add(NumberOfDays);
            return (newDateTime );
            }

    }
}