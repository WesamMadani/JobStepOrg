﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyProject.Models.VM;
using MyProject.Models;

namespace MyProject.Controllers
{
    public class HomeController : Controller
    {
        private JobStepEntities db = new JobStepEntities();
        public ActionResult Index()
        {
            Home VM = new Home()
            {
                Employee = db.Employees.ToList(),
                News = db.News.Where(m => m.IsActive == true).OrderByDescending(o => o.PublishDate).ToList(),
                About = db.GeneralDatas.Where(m => m.C_Key == "About").SingleOrDefault(),
                Adress = db.GeneralDatas.Where(m => m.C_Key == "Adress").SingleOrDefault(),
                Phone = db.GeneralDatas.Where(m => m.C_Key == "Phone").SingleOrDefault(),
                Email = db.GeneralDatas.Where(m => m.C_Key == "Email").SingleOrDefault(),
                Skype = db.GeneralDatas.Where(m => m.C_Key == "Skype").SingleOrDefault(),
                MISSION = db.GeneralDatas.Where(m => m.C_Key == "MISSION").SingleOrDefault(),
                VISION = db.GeneralDatas.Where(m => m.C_Key == "VISION").SingleOrDefault(),
                OBJECTIVES = db.GeneralDatas.Where(m => m.C_Key == "OBJECTIVES").SingleOrDefault(),
                HoldPlace1 = db.Advertisments.Where(m => m.PostionID == 1 & m.IsActive == true).OrderByDescending(o => o.PublishDate).FirstOrDefault(),
                HoldPlace2 = db.Advertisments.Where(m => m.PostionID == 2 & m.IsActive == true).OrderByDescending(o => o.PublishDate).FirstOrDefault(),
                Client = db.Clients.Where(m => m.IsActive == true).OrderBy(m=>m.Name).Skip(1).ToList(),
                FClient = db.Clients.Where(m => m.IsActive == true).FirstOrDefault(),
                Partner = db.Partners.Where(m => m.IsActive == true).ToList(),
                Project = db.Projects.Where(m => m.IsActive == true).ToList(),
            };
           
            if (VM.News.Count() > 5)
            {
                VM.News= VM.News.Take(5).ToList();
                        }
            if (VM.Employee.Count() > 3)
            {
                VM.Employee = VM.Employee.Take(3).ToList();
            }
            if (VM.Client.Count() > 4)
            {
                VM.Client = VM.Client.Take(4).ToList();
            }

            return View(VM);
        }
        public ActionResult OneNew(int id)
        {

            Home VM = new Home()
            {

                News = db.News.Where(m => m.IsActive == true).OrderByDescending(o => o.PublishDate).ToList(),
                New = db.News.Where(m => m.NewsID == id).SingleOrDefault()
        };
            if (VM.News.Count() > 3)
            {
                VM.News = VM.News.Take(3).ToList();
            }

            return View(VM);
        }
        public ActionResult Gallery()
        {
           List <News> News = db.News.Where(m => m.IsActive == true).OrderByDescending(o => o.PublishDate).ToList();
            return View(News);
        }
        [HttpPost]
         public ActionResult Add_email(string mail )
        {
            MailList email = new MailList();
            email.Email = mail;
            email.SubscribeDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.MailLists.Add(email);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           

            return RedirectToAction("Index");
        }
        //    public ActionResult About()
        //    {
        //        ViewBag.Message = "Your application description page.";

        //        return View();
        //    }

        //    public ActionResult Contact()
        //    {
        //        ViewBag.Message = "Your contact page.";

        //        return View();
        //    }
    }
}