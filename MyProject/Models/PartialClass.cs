﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyProject.Models
{
    [MetadataType(typeof(EmployeeMetadata))]
    public partial class Employee
    {

    }
    [MetadataType(typeof(AdvertismentMetadata))]
    public partial class Advertisment
    {

    }

    [MetadataType(typeof(ClientMetadata))]
    public partial class Client
    {

    }
    [MetadataType(typeof(GeneralDataMetadata))]
    public partial class GeneralData
    {

    }
    [MetadataType(typeof(ImageMetadata))]
    public partial class Image
    {

    }
    [MetadataType(typeof(MailListMetadata))]
    public partial class MailList
    {

    }

    [MetadataType(typeof(NewsMetadata))]
    public partial class News
    {

    }
    [MetadataType(typeof(PartnerMetadata))]
    public partial class Partner
    {

    }
    [MetadataType(typeof(ProjectMetadata))]
    public partial class ProjectMetadata
    {

    }
    [MetadataType(typeof(TestimonialMetadata))]
    public partial class Testimonial
    {

    }

}