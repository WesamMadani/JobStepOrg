﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyProject.Models
{
    public class EmployeeMetadata
    {
        public int EmployeeID { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public string Summery { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        //[AllowHtml]
        //public string Photo { get; set; }
    }
    public class AdvertismentMetadata
    {

        [Required]
        [StringLength(500)]
        
        public string Title { get; set; }
        [Required]
        [StringLength(50)]
        public string image { get; set; }
        [StringLength(500)]
        [Url]
        public string URL { get; set; }

    }
    public class ClientMetadata
    {
        [EmailAddress]
        public string Email { get; set; }
        [Column(TypeName = "int")]
        public int PhoneNO { get; set; }

    }
    public class GeneralDataMetadata
    {
        [Required]
        public string C_Key { get; set; }
        [Required]
        public string C_Value { get; set; }

    }

    public class ImageMetadata
    {
         
        [Required]
        [StringLength(100)]
        public string ImageName { get; set; }
        [Display(Name = "Publish Date")]

        public DateTime PublishDate { get; set; }

    }
    public partial class MailListMetadata
    {
        [Required]
        [StringLength(50)]
        [EmailAddress]
        public string Email { get; set; }
        [Display(Name = "Subscribe Date")]

        public DateTime SubscribeDate { get; set; }

    }
    public partial class NewsMetadata
    {
        public int NewsID { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string Details { get; set; }

        [Required]
        [StringLength(50)]
        public string Image { get; set; }
        [Display(Name = "Publish Date")]

        public DateTime PublishDate { get; set; }
        public bool IsActive { get; set; }
        [Display(Name = "Category")]

        public int CategoryID { get; set; }

        [Display(Name = "Employee ID")]

        public int EmployeeID { get; set; }

    }
    public partial class PartnerMetadata
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        
        public int PhoneNO { get; set; }
        [Required]
        [StringLength(50)]
        public string Image { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Image Black And Whit")]
        public string ImageBlackAndWhit { get; set; }
    }
    public partial class ProjectMetadata
    {
        [Required]
        public string ProjectName { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        [Display(Name = "Partner")]
        public int PartnerID { get; set; }
        [Required]
        [Display(Name = "Client")]
        public int ClientID { get; set; }


    }

    public partial class TestimonialMetadata
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Email { get; set; }
        [Required]
        [Column(TypeName = "ntext")]
        public string Text { get; set; }
        [Display(Name = "Publish Date")]
        public System.DateTime PublishDate { get; set; }
    }


}



