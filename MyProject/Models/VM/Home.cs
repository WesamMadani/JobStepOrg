﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyProject.Models.VM
{
    public class Home
    {
        public News New { get; set; }
        public List<News> News { get; set; }
        public List<JobAnnouncement> JobAnnouncement { get; set; }
        public List<Employee> Employee { get; set; }
        public Advertisment HoldPlace1 { get; set; }
        public Advertisment HoldPlace2 { get; set; }
        public List<Image> Images { get; set; }
        public GeneralData About { get; set; }
        public GeneralData Adress { get; set; }
        public GeneralData Phone { get; set; }
        public GeneralData Email { get; set; }
        public GeneralData Skype { get; set; }
        public GeneralData MISSION { get; set; }
        public GeneralData VISION { get; set; }
        public GeneralData OBJECTIVES { get; set; }
        public List<Client> Client { get; set; }
        public Client FClient { get; set; }
        public List<Project> Project { get; set; }
        public List<Partner> Partner { get; set; }
    }


}
