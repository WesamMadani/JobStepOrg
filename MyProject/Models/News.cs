//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyProject.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class News
    {
        public int NewsID { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public string Image { get; set; }
        public System.DateTime PublishDate { get; set; }
        public bool IsActive { get; set; }
        public int CategoryID { get; set; }
        public int EmployeeID { get; set; }
    
        public virtual Categotire Categotire { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
