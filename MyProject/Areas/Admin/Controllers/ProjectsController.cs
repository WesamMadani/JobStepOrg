﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyProject.Models;

namespace MyProject.Areas.Admin.Controllers
{
    public class ProjectsController : Controller
    {
        private JobStepEntities db = new JobStepEntities();

        // GET: Admin/Projects
        public ActionResult Index()
        {
            var projects = db.Projects.Include(p => p.Client).Include(p => p.Partner);
            return View(projects.ToList());
        }

        // GET: Admin/Projects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // GET: Admin/Projects/Create
        public ActionResult Create()
        {
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name");
            ViewBag.PartnerID = new SelectList(db.Partners, "PartnerID", "Name");
            return View();
        }

        // POST: Admin/Projects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "ProjectID,ProjectName,description,PartnerID,ClientID,Image,Duration,IsActive")] Project project,HttpPostedFileBase Image)
        {
            string filename = Helper.HelpFunctions.CreateNameImage(Image);
           
            if (filename == "" )
            {
                ViewBag.ImageError = "Error Extenstion we accetp jpg or png or gif only";
                return View(project);
            }
            Image.SaveAs(Server.MapPath("~/Uploads/project/" + filename));
            project.Image = filename;
            project.StartDate = DateTime.Now;
            project.EndDate = Helper.HelpFunctions.EndDate(project.Duration);
            if (ModelState.IsValid)
            {
                db.Projects.Add(project);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name", project.ClientID);
            ViewBag.PartnerID = new SelectList(db.Partners, "PartnerID", "Name", project.PartnerID);
            return View(project);
        }

        // GET: Admin/Projects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name", project.ClientID);
            ViewBag.PartnerID = new SelectList(db.Partners, "PartnerID", "Name", project.PartnerID);
            return View(project);
        }

        // POST: Admin/Projects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "ProjectID,ProjectName,description,PartnerID,ClientID,Image,Duration,IsActive")] Project project, HttpPostedFileBase Image)
        {
            if (Image != null)
            {
                string filename = Helper.HelpFunctions.CreateNameImage(Image);
                if (filename == "")
                {
                    ViewBag.ImageError = "Error Extenstion we accetp jpg or png or gif only";
                    return View(project);
                }
                Image.SaveAs(Server.MapPath("~/Uploads/project/" + filename));
                //delete file from server
                string old_file_name = project.Image;
                var uri = new Uri(Server.MapPath("~/Uploads/project/" + old_file_name));
                System.IO.File.Delete(uri.LocalPath);
                //add new file to database
                project.StartDate = db.Projects.Where(m => m.ProjectID == project.ProjectID).Select(s => s.StartDate).Single();
                project.Image = filename;
            }
            project.EndDate = Helper.HelpFunctions.EndDate(project.Duration);
            if (ModelState.IsValid)
            {
                db.Entry(project).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name", project.ClientID);
            ViewBag.PartnerID = new SelectList(db.Partners, "PartnerID", "Name", project.PartnerID);
            return View(project);
        }

        // GET: Admin/Projects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: Admin/Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Project project = db.Projects.Find(id);
            
                //to catch the photo name
                var photoName = "";
                photoName = project.Image;
                db.Projects.Remove(project);
                db.SaveChanges();

                //delet the photo

                string fullPath = Request.MapPath("~/Uploads/project/" + photoName);
               
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);

                }
                
                return RedirectToAction("Index");
            
           
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
