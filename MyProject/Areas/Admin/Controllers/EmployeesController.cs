﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyProject.Models;
using PagedList;

namespace MyProject.Areas.Admin
{
    public class EmployeesController : Controller
    {
        private JobStepEntities db = new JobStepEntities();

        // GET: Admin/Employees
        public ActionResult Index(string searchBy, string search, int? page)
        {
            var data = db.Employees.AsQueryable();
            if (searchBy == "Name")
            {
                data = data.Where(x => x.Name.Contains(search));
            }
            if (searchBy == "Email")
            {
                data = data.Where(x => x.Email == search);
            }

            var model = data.ToList().ToPagedList(page ?? 1, 5);
            return View(model);


        }
        // GET: Admin/Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Admin/Employees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmployeeID,Name,Summery,Email,Image")] Employee employee, HttpPostedFileBase Image)
        {

            string guid = Guid.NewGuid().ToString("N").ToLower();
            string Extenstion = Image.FileName.Split('.')[1];
            if (!(Extenstion == "jpg" || Extenstion == "png" || Extenstion == "gif"))
            {
                return View(employee);
            }
            string filename = guid + '.' + Extenstion;
            Image.SaveAs(Server.MapPath("~/Uploads/employee/" + filename));
            employee.Image = filename;

            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employee);
        }

        // GET: Admin/Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Admin/Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "EmployeeID,Name,Summery,Email,Image")] Employee employee, HttpPostedFileBase Image)
        {
            if (Image != null)
            {


                string guid = Guid.NewGuid().ToString("N").ToLower();
                string Extenstion = Image.FileName.Split('.')[1];
                if (!(Extenstion == "jpg" || Extenstion == "png" || Extenstion == "gif"))
                {
                    return View(employee);
                }
                string filename = guid + '.' + Extenstion;
                Image.SaveAs(Server.MapPath("~/Uploads/employee/" + filename));
                //delete file from server
                string old_file_name = employee.Image;
                var uri = new Uri(Server.MapPath("~/Uploads/employee/" + old_file_name));
                System.IO.File.Delete(uri.LocalPath);
                //add new file to database
                employee.Image = filename;

            }
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        // GET: Admin/Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Admin/Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);

            News news = db.News.Where(x => x.EmployeeID == id).FirstOrDefault();
            if (news != null)
            {
                ViewBag.error = " Sorry there is a new  has  relation with this Employee you can't delet it, You must delete the associated news first";


                return View(employee);
            }
            else
            {
                var photoName = "";
                photoName = employee.Image;

                db.Employees.Remove(employee);
                db.SaveChanges();

                //delet the photo

                string fullPath = Request.MapPath("~/Uploads/employee/"
                + photoName);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);

                }


                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
