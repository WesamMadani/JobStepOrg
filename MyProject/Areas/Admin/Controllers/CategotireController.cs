﻿using MyProject.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MyProject.Areas.Admin.Controllers
{
    public class CategotireController : Controller
    {
        private JobStepEntities db = new JobStepEntities();
        public ActionResult Index()
        {
            return View(db.Categotires.ToList());
        }
        
        // GET: Admin/Categotires/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categotire categotire = db.Categotires.Find(id);
            if (categotire == null)
            {
                return HttpNotFound();
            }
            return View(categotire);
        }

        // GET: Admin/Categotires/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Categotires/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoreyID,Name")] Categotire categotire)
        {
            if (ModelState.IsValid)
            {
                db.Categotires.Add(categotire);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categotire);
        }

        // GET: Admin/Categotires/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categotire categotire = db.Categotires.Find(id);
            if (categotire == null)
            {
                return HttpNotFound();
            }
            return View(categotire);
        }

        // POST: Admin/Categotires/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoreyID,Name")] Categotire categotire)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categotire).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categotire);
        }

        // GET: Admin/Categotires/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categotire categotire = db.Categotires.Find(id);
            if (categotire == null)
            {
                return HttpNotFound();
            }
            return View(categotire);
        }

        // POST: Admin/Categotires/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Categotire categotire = db.Categotires.Find(id);
            News Catnew= db.News.Where(x => x.CategoryID == id).FirstOrDefault();
            if(Catnew!=null)
                {
                ViewBag.error = " Sorry there is a new  has  relation with this Categotire you can't delet itو You must delete the associated news first";
                
               
                return View(categotire);
            }
            else  {
                db.Categotires.Remove(categotire);
                db.SaveChanges();
                ViewBag.error = "";
                return RedirectToAction("Index");

            }

           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}