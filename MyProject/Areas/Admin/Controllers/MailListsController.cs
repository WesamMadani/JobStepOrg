﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyProject.Models;

namespace MyProject.Areas.Admin.Controllers
{
    public class MailListsController : Controller
    {
        private JobStepEntities db = new JobStepEntities();

        // GET: Admin/MailLists
        public ActionResult Index()
        {
            return View(db.MailLists.ToList());
        }

        // GET: Admin/MailLists/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MailList mailList = db.MailLists.Find(id);
            if (mailList == null)
            {
                return HttpNotFound();
            }
            return View(mailList);
        }

        // GET: Admin/MailLists/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/MailLists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MailLIstID,Email,SubscribeDate,Status")] MailList mailList)
        {
            if (ModelState.IsValid)
            {
                db.MailLists.Add(mailList);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mailList);
        }

        // GET: Admin/MailLists/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MailList mailList = db.MailLists.Find(id);
            if (mailList == null)
            {
                return HttpNotFound();
            }
            return View(mailList);
        }

        // POST: Admin/MailLists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MailLIstID,Email,SubscribeDate,Status")] MailList mailList)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mailList).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mailList);
        }

        // GET: Admin/MailLists/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MailList mailList = db.MailLists.Find(id);
            if (mailList == null)
            {
                return HttpNotFound();
            }
            return View(mailList);
        }

        // POST: Admin/MailLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MailList mailList = db.MailLists.Find(id);
            db.MailLists.Remove(mailList);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
