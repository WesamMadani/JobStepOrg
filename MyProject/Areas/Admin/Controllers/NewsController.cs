﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyProject.Models;
using PagedList;

namespace MyProject.Areas.Admin.Controllers
{
    
    public class NewsController : Controller
    {
        
        private JobStepEntities db = new JobStepEntities();
        // GET: Admin/News
        public ActionResult Index(string searchBy, string search, int? page)
        {
            var data = db.News.Include(n => n.Employee).Include(n => n.Categotire).OrderByDescending(x => x.PublishDate).AsQueryable();
            if (searchBy == "Title")
            {
                data = data.Where(x => x.Title.Contains(search));
            }
            if (searchBy == "Categotire")
            {
                data = data.Where(x => x.Categotire.Name.Contains(search));
            }
           
            var model = data.ToList().ToPagedList(page ?? 1, 5);
            return View(model);


            //var news = db.News.Include(n => n.Employee).Include(n => n.Categotire);
            //return View(news.ToList());
        }

        // GET: Admin/News/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }
        // GET: Admin/News/Create
        public ActionResult Create()
        {
            ViewBag.EmployeeID = new SelectList(db.Employees, "EmployeeID", "Name");
            ViewBag.CategoryID = new SelectList(db.Categotires, "CategoreyID", "Name");
            return View();
        }

        // POST: Admin/News/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(
            [Bind(Include = "NewsID,Title,Details,IsActive,CategoryID,EmployeeID,Image")] News news, HttpPostedFileBase Image)
        {
            ViewBag.EmployeeID = new SelectList(db.Employees, "EmployeeID", "Name", news.EmployeeID);
            ViewBag.CategoryID = new SelectList(db.Categotires, "CategoreyID", "Name", news.CategoryID);

            string guid = Guid.NewGuid().ToString("N").ToLower();
            string Extenstion = Image.FileName.Split('.')[1];
            if (!(Extenstion == "jpg" || Extenstion == "png" || Extenstion == "gif"))
            {
                return View(news);
            }
            string filename = guid + '.' + Extenstion;
            Image.SaveAs(Server.MapPath("~/Uploads/" + filename));
            news.Image = filename;
            news.PublishDate = DateTime.Now;

            //  ModelState[0].Errors[0].ErrorMessage
            if (ModelState.IsValid)
            {

                db.News.Add(news);
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            return View(news);
        }

        // GET: Admin/News/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmployeeID = new SelectList(db.Employees, "EmployeeID", "Name", news.EmployeeID);
            ViewBag.CategoryID = new SelectList(db.Categotires, "CategoreyID", "Name", news.CategoryID);
            return View(news);
        }

        // POST: Admin/News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NewsID,Title,Details,Image,PublishDate,IsActive,CategoryID,EmployeeID")] News news, HttpPostedFileBase Image)
        {

           
            if (Image != null)
            {
                string guid = Guid.NewGuid().ToString("N").ToLower();
                string Extenstion = Image.FileName.Split('.')[1];
                if (!(Extenstion == "jpg" || Extenstion == "png" || Extenstion == "gif"))
                {
                    return View(news);
                }
                string filename = guid + '.' + Extenstion;
                Image.SaveAs(Server.MapPath("~/Uploads/" + filename));
                //delete file from server
                string old_file_name = news.Image;
                var uri = new Uri(Server.MapPath("~/Uploads/" + old_file_name));
                System.IO.File.Delete(uri.LocalPath);
                //add new file to database
                news.Image = filename;

            }
            if (ModelState.IsValid)
            {
                db.Entry(news).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EmployeeID = new SelectList(db.Employees, "EmployeeID", "Name", news.EmployeeID);
            ViewBag.CategoryID = new SelectList(db.Categotires, "CategoreyID", "Name", news.CategoryID);
            return View(news);
        }

        // GET: Admin/News/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: Admin/News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            News news = db.News.Find(id);
            //to catch the photo name
            var photoName = "";
            photoName = news.Image;

            db.News.Remove(news);
            db.SaveChanges();

            //delet the photo
           
            string fullPath = Request.MapPath("~/Uploads/"
            + photoName);

            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
               
            }


            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
