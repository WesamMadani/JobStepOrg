﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyProject.Models;

namespace MyProject.Areas.Admin.Controllers
{
    public class AdvertismentsController : Controller
    {
        private JobStepEntities db = new JobStepEntities();

        // GET: Admin/Advertisments
        public ActionResult Index()
        {
            return View(db.Advertisments.ToList());
        }

        // GET: Admin/Advertisments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisment advertisment = db.Advertisments.Find(id);
            if (advertisment == null)
            {
                return HttpNotFound();
            }
            return View(advertisment);
        }

        // GET: Admin/Advertisments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Advertisments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AdvertismentID,Title,image,URL,PostionID,IsActive,PublishDate,FormDate,ToDate")] Advertisment advertisment,HttpPostedFileBase image)
        {

            string guid = Guid.NewGuid().ToString("N").ToLower();
            string Extenstion = image.FileName.Split('.')[1];
            if (!(Extenstion == "jpg" || Extenstion == "png" || Extenstion == "gif"))
            {
                return View(advertisment);
            }
            string filename = guid + '.' + Extenstion;
            image.SaveAs(Server.MapPath("~/Uploads/advertisment/" + filename));
            advertisment.image = filename;
            advertisment.PublishDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Advertisments.Add(advertisment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(advertisment);
        }

        // GET: Admin/Advertisments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisment advertisment = db.Advertisments.Find(id);
            if (advertisment == null)
            {
                return HttpNotFound();
            }
            return View(advertisment);
        }

        // POST: Admin/Advertisments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AdvertismentID,Title,image,URL,PostionID,IsActive,PublishDate,FormDate,ToDate")] Advertisment advertisment, HttpPostedFileBase Image)
        {
            if (Image != null)
            {
                string guid = Guid.NewGuid().ToString("N").ToLower();
                string Extenstion = Image.FileName.Split('.')[1];
                if (!(Extenstion == "jpg" || Extenstion == "png" || Extenstion == "gif"))
                {
                    return View(advertisment);
                }
                string filename = guid + '.' + Extenstion;
                Image.SaveAs(Server.MapPath("~/Uploads/advertisment/" + filename));
                //delete file from server
                string old_file_name = advertisment.image;
                var uri = new Uri(Server.MapPath("~/Uploads/advertisment/" + old_file_name));
                System.IO.File.Delete(uri.LocalPath);
                //add new file to database
                advertisment.image = filename;
            }
            if (ModelState.IsValid)
            {
                db.Entry(advertisment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(advertisment);
        }

        // GET: Admin/Advertisments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisment advertisment = db.Advertisments.Find(id);
            if (advertisment == null)
            {
                return HttpNotFound();
            }
            return View(advertisment);
        }

        // POST: Admin/Advertisments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Advertisment advertisment = db.Advertisments.Find(id);
            //to catch the photo name
            var photoName = "";
            photoName = advertisment.image;

            db.Advertisments.Remove(advertisment);
            db.SaveChanges();
            //delet the photo

            string fullPath = Request.MapPath("~/Uploads/advertisment/"
            + photoName);

            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);

            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
