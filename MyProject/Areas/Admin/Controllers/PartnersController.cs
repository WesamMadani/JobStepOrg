﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyProject.Models;

namespace MyProject.Areas.Admin.Controllers
{
    public class PartnersController : Controller
    {
        private JobStepEntities db = new JobStepEntities();

        // GET: Admin/Partners
        public ActionResult Index()
        {
            return View(db.Partners.ToList());
        }

        // GET: Admin/Partners/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Partner partner = db.Partners.Find(id);
            if (partner == null)
            {
                return HttpNotFound();
            }
            return View(partner);
        }

        // GET: Admin/Partners/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Partners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "PartnerID,Name,Summery,Email,Image,PhoneNO,IsActive,ImageBlackAndWhit")] Partner partner, HttpPostedFileBase Image, HttpPostedFileBase ImageBlackAndWhit)
        {
          
            string filename = Helper.HelpFunctions.CreateNameImage(Image);
            string filenameBAndW = Helper.HelpFunctions.CreateNameImage(ImageBlackAndWhit);
            if (filename=="" || filenameBAndW == "")
                {
                ViewBag.ImageError = "Error Extenstion we accetp jpg or png or gif only";
                return View(partner);
            }
               
            Image.SaveAs(Server.MapPath("~/Uploads/partner/" + filename));
            ImageBlackAndWhit.SaveAs(Server.MapPath("~/Uploads/partner/ImageBlackAndWhit/" + filenameBAndW));
            partner.Image = filename;
            partner.ImageBlackAndWhit = filenameBAndW;
            if (ModelState.IsValid)
            {
                db.Partners.Add(partner);
            db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(partner);
        }

        // GET: Admin/Partners/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Partner partner = db.Partners.Find(id);
            if (partner == null)
            {
                return HttpNotFound();
            }
            return View(partner);
        }

        // POST: Admin/Partners/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "PartnerID,Name,Summery,Email,Image,PhoneNO,IsActive,ImageBlackAndWhit")] Partner partner, HttpPostedFileBase Image, HttpPostedFileBase ImageBlackAndWhit)
        {
            if (Image != null)
            {
                string filename = Helper.HelpFunctions.CreateNameImage(Image);
                if (filename == "")
                {
                    ViewBag.ImageError = "Error Extenstion we accetp jpg or png or gif only";
                    return View(partner);
                }
                Image.SaveAs(Server.MapPath("~/Uploads/partner/" + filename));
                //delete file from server
                string old_file_name = partner.Image;
                var uri = new Uri(Server.MapPath("~/Uploads/partner/" + old_file_name));
                System.IO.File.Delete(uri.LocalPath);
                //add new file to database
                partner.Image = filename;
            }
            if (ImageBlackAndWhit != null)
            {
                string filenameBAndW = Helper.HelpFunctions.CreateNameImage(ImageBlackAndWhit);
                if (filenameBAndW == "")
                {
                    ViewBag.ImageError = "Error Extenstion we accetp jpg or png or gif only";
                    return View(partner);
                }
                ImageBlackAndWhit.SaveAs(Server.MapPath("~/Uploads/partner/ImageBlackAndWhit/" + filenameBAndW));
                //delete file from server
                string old_file_name = partner.ImageBlackAndWhit;
                var uri = new Uri(Server.MapPath("~/Uploads/partner/ImageBlackAndWhit/" + old_file_name));
                System.IO.File.Delete(uri.LocalPath);
                //add new file to database
                partner.ImageBlackAndWhit = filenameBAndW;
                
            }
           
            if (ModelState.IsValid)
            {
                db.Entry(partner).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(partner);
        }

        // GET: Admin/Partners/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Partner partner = db.Partners.Find(id);
            if (partner == null)
            {
                return HttpNotFound();
            }
            return View(partner);
        }

        // POST: Admin/Partners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Partner partner = db.Partners.Find(id);
            Project PartnerPr = db.Projects.Where(x => x.PartnerID == id).FirstOrDefault();
            if (PartnerPr != null)
            {
                ViewBag.error = " Sorry there is a Project  has  relation with this partner you can't delet itو You must delete the associated Project first";


                return View(partner);
            }
            else
            {
                //to catch the photo name
                var photoName = ""; var photoBlackAndWhit = "";
                photoName = partner.Image;
                photoBlackAndWhit = partner.ImageBlackAndWhit;
                db.Partners.Remove(partner);
                db.SaveChanges();
                
                //delet the photo

                string fullPath = Request.MapPath("~/Uploads/partner/"+ photoName);
                string BlackAndWhitfullPath = Request.MapPath("~/Uploads/partner/ImageBlackAndWhit/" + photoName);
               
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);

                }
                if (System.IO.File.Exists(BlackAndWhitfullPath))
                {
                    System.IO.File.Delete(BlackAndWhitfullPath);

                }
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
