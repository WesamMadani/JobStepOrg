﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyProject.Models;

namespace MyProject.Areas.Admin.Controllers
{
    public class ClientsController : Controller
    {
        private JobStepEntities db = new JobStepEntities();

        // GET: Admin/Clients
        public ActionResult Index()
        {
            return View(db.Clients.ToList());
        }

        // GET: Admin/Clients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // GET: Admin/Clients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "ClientID,Name,Summery,Email,PhoneNO,IsActive,image")] Client client, HttpPostedFileBase image)
        {
            string guid = Guid.NewGuid().ToString("N").ToLower();
            string Extenstion = image.FileName.Split('.')[1];
            if (!(Extenstion == "jpg" || Extenstion == "png" || Extenstion == "gif"))
            {
                return View(client);
            }
            string filename = guid + '.' + Extenstion;
            image.SaveAs(Server.MapPath("~/Uploads/client/" + filename));
            client.image = filename;
            
            if (ModelState.IsValid)
            {
                db.Clients.Add(client);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(client);
        }

        // GET: Admin/Clients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // POST: Admin/Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "ClientID,Name,Summery,Email,PhoneNO,IsActive,image")] Client client, HttpPostedFileBase image)
        {
            if (image != null)
            {
                string guid = Guid.NewGuid().ToString("N").ToLower();
                string Extenstion = image.FileName.Split('.')[1];
                if (!(Extenstion == "jpg" || Extenstion == "png" || Extenstion == "gif"))
                {
                    return View(client);
                }
                string filename = guid + '.' + Extenstion;
                image.SaveAs(Server.MapPath("~/Uploads/client/" + filename));
                //delete file from server
                string old_file_name = client.image;
                var uri = new Uri(Server.MapPath("~/Uploads/client/" + old_file_name));
                System.IO.File.Delete(uri.LocalPath);
                //add new file to database
                client.image = filename;
            }
            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(client);
        }

        // GET: Admin/Clients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // POST: Admin/Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            Client client = db.Clients.Find(id);
            Project ClientPr = db.Projects.Where(x => x.ClientID == id).FirstOrDefault();
            if (ClientPr != null)
            {
                ViewBag.error = " Sorry there is a Project  has  relation with this client you can't delet itو You must delete the associated Project first";


                return View(client);
            }
            else
            {
                //to catch the photo name
                var photoName = "";
                photoName = client.image;

                db.Clients.Remove(client);
                db.SaveChanges();
                //delet the photo

                string fullPath = Request.MapPath("~/Uploads/client/"
                + photoName);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);

                }
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
